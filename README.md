<h2>The Movie Detective</h2>
Use the Movie Detective to get movie suggestions!

PlayStore Link:
<a href="https://play.google.com/store/apps/details?id=com.kingdinkus.actionbar"> Click here release!</a>

Features (For those to impatient to watch a video!)

<ul>
  <li>Touch Discover buttons to find the movie you want to watch</li>
  <li>That didn't help? Get Recommended Titles by searching a movie/show you liked </li>
  <li>See whats Most Popular on Netflix and Recently Added</li>
  <li>Skim over Rotten Tomatoes, IMDB, and Metacritic ratings for a quick quality overview</li>
  <li>View Trailers to Movies and TV shows for a visual summary</li>
  <li>Can't remember a movie title? Search the director or an actor and glance at what their most known for</li>
  <li>Connect with famous Directors and Actors by being pointed to their official social media pages (Facebook, Twitter, and Instagram)</li>
  <li>Use our advanced search engine to find movies by Rating, Popularity and Genre.</li>
  <li>Stay updated on Movie News with the News Tab!</li>
 </ul>
  
<img src="https://github.com/jbonino/android-movie-detective/blob/master/FlickDic/gif.gif" height="500"/>

Click video below to see all its features!

[![Alt text for your video](https://img.youtube.com/vi/Sqd2QrV1Tm4/0.jpg)](https://www.youtube.com/watch?v=Sqd2QrV1Tm4)
